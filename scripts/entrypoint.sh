#!/bin/sh

CONTAINER_FIRST_STARTUP="CONTAINER_FIRST_STARTUP"
if [ ! -e /$CONTAINER_FIRST_STARTUP ]; then
    touch /$CONTAINER_FIRST_STARTUP

    # Install GO
    wget https://go.dev/dl/go${GOLANG_VERSION}.linux-amd64.tar.gz
    rm -rf /usr/local/go && tar -C /usr/local -xzf go${GOLANG_VERSION}.linux-amd64.tar.gz
    rm -rf go${GOLANG_VERSION}.linux-amd64.tar.gz

    # Install NODEJS
    su - app -c "nvm install ${NODEJS_VERSION}"
    su - app -c "nvm alias default ${NODEJS_VERSION}"
    su - app -c "nvm use ${NODEJS_VERSION}"

    # Setup PGAdmin
    /usr/pgadmin4/bin/setup-web.sh --yes

    service postgresql start
    psql -U postgres -c "ALTER USER postgres WITH PASSWORD '$POSTGRES_ROOT_PASSWORD'"

    nohup apache2ctl -D FOREGROUND &
    /usr/sbin/sshd -D
else
    service postgresql start

    nohup apache2ctl -D FOREGROUND &
    /usr/sbin/sshd -D
fi