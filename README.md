<div align="center">

![](/assets/logo_compressed.png "docker-go-node-pg")

<br>

![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white) ![Ubuntu](https://img.shields.io/badge/Ubuntu-E95420?style=for-the-badge&logo=ubuntu&logoColor=white) ![Go](https://img.shields.io/badge/go-%2300ADD8.svg?style=for-the-badge&logo=go&logoColor=white) ![NodeJS](https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white) ![NPM](https://img.shields.io/badge/NPM-%23000000.svg?style=for-the-badge&logo=npm&logoColor=white) ![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white)

<br>
<a href="#"><img src="https://img.shields.io/badge/ubuntu-20.04-red"></a>
<a href="#"><img src="https://img.shields.io/badge/go-1.19.4-red"></a>
<a href="#"><img src="https://img.shields.io/badge/nodejs-18.12.1-red"></a>
<a href="#"><img src="https://img.shields.io/badge/npm-9.19.2-red"></a>
<a href="#"><img src="https://img.shields.io/badge/postgres-12.12-red"></a>
<a href="https://paypal.me/rodrigoodhin"><img src="https://img.shields.io/badge/donate-PayPal-blue"></a>
<br><br>
</div>

# Docker + GO + NODE + POSTGRES

A Docker image with a linux environment prepared with the the main features for develop in GO and Javascript using Postgres as database.

&nbsp;
&nbsp;
&nbsp;

## Download image from Docker Hub

```shell
docker pull rodrigoodhin/go-node-pg
```

&nbsp;
&nbsp;
&nbsp;

## Create a new image

```shell
docker build -t <YOUR_DOCKER_HUB_USER>/go-node-pg . 
```

&nbsp;
&nbsp;
&nbsp;

## Push created image to docker hub

```shell
docker login
docker push <YOUR_DOCKER_HUB_USER>/go-node-pg
```

&nbsp;
&nbsp;
&nbsp;

## Using docker-compose.yml

*To use this image within your project, copy the file `docker-compose.yml` to your project's folder and execute the steps below.*

#### Start services

```shell
docker-compose up -d
```

#### List containers to get SSH port

```shell
docker container ls
```

#### Open SSH connection

*The password is: app*

```shell
ssh app@localhost -p <SSH_PORT>
```

&nbsp;
&nbsp;
&nbsp;

## Running a GO code inside the container

*The /app folder inside the container is a link for the folder where do you run the docker-compose command*

1- Open a SSH connection the the container

2- Go to the folder /app

3- Execute your GO code


&nbsp;
&nbsp;
&nbsp;

## Using pgAdmin4 to access the Postgre database

1- Open the url https://localhost:38080/pdadmin4

2- Login with "rodrigoodhin@gmail" as email and "rodrigoodhin" as password

3- Click in the "Add New Server" button

4- In the "General" tab, set the name of your connection

5- In the "Connection" tab, set "localhost" as host name, "5432" as Port, "postgres" as Username and "rodrigoodhin" as Password

6- Click in the "Save" button

&nbsp;
&nbsp;
&nbsp;

## LICENSE

[MIT License](/LICENSE)
