FROM ubuntu:20.04
RUN rm /bin/sh && ln -s /bin/bash /bin/sh
SHELL ["/bin/bash", "-c"]

LABEL maintainer="Rodrigo Odhin"

ARG DEBIAN_FRONTEND=noninteractive

# Update Ubuntu
RUN apt-get -y update
RUN apt-get -y upgrade

# Install base dependencies
RUN apt-get install -y apt-transport-https build-essential ca-certificates openssh-server --fix-missing
RUN apt-get install -y curl sudo gpg git vim libssl-dev wget gcc g++ make apt-utils --fix-missing
RUN rm -rf /var/lib/apt/lists/*

# Install Postgres
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
RUN apt-get update
RUN apt-get install -y postgresql postgresql-contrib
RUN sed -i  '/^local   all             postgres                                peer/ s/peer/trust/' /etc/postgresql/12/main/pg_hba.conf

# Install pgAdmin4
RUN curl https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo apt-key add
RUN sh -c 'echo "deb https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main" > /etc/apt/sources.list.d/pgadmin4.list && apt update'
RUN apt-get install -y pgadmin4-web

# Configure SSH
RUN mkdir /var/run/sshd
RUN echo 'root:root' |chpasswd
RUN sed -ri 's/^PermitRootLogin\s+.*/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config
RUN echo 'Banner /etc/banner' >> /etc/ssh/sshd_config

COPY etc/banner /etc/

RUN useradd -ms /bin/bash app
RUN adduser app sudo
RUN echo 'app:app' |chpasswd

USER app

# Install NVM
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.2/install.sh | bash
RUN echo "export NVM_DIR=\$HOME/.nvm" >> ~/.profile
RUN echo "[ -s \"\$NVM_DIR/nvm.sh\" ] && \. \"\$NVM_DIR/nvm.sh\"" >> ~/.profile
RUN echo "[ -s \"\$NVM_DIR/bash_completion\" ] && . \"\$NVM_DIR/bash_completion\"" >> ~/.profile

# Configure Go environment
RUN echo "export PATH=$PATH:/usr/local/go/bin" >> ~/.bashrc

USER root

EXPOSE 22
EXPOSE 80
EXPOSE 5432
EXPOSE 8787

RUN mkdir /Users
RUN mkdir /app

RUN mkdir /scripts
COPY scripts/entrypoint.sh /scripts/entrypoint.sh
RUN ["chmod", "777", "/scripts/entrypoint.sh"]
ENTRYPOINT ["/scripts/entrypoint.sh"]
